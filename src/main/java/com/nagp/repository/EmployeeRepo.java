package com.nagp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nagp.model.Employee;


@Repository
public interface EmployeeRepo extends JpaRepository<Employee, String> {
	 

}
